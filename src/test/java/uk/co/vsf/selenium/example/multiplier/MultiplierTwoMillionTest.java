package uk.co.vsf.selenium.example.multiplier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.co.vsf.selenium.example.Browser;
import uk.co.vsf.selenium.example.page.IndexPage;
import uk.co.vsf.selenium.example.page.MultiplierPage;

public class MultiplierTwoMillionTest {

    private static Browser BROWSER;

    private final IndexPage indexPage = new IndexPage(BROWSER);

    @BeforeClass
    public static void beforeClass() {
        // BROWSER = Browser.local(Type.FIREFOX);
        BROWSER = Browser.instance();
    }

    @Before
    public void before() {
        BROWSER.navigateTo(indexPage);
    }

    @Test
    public void millionMultiplier_1() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("1000000")).enterValue2(new BigDecimal("30.00")).andCalculate();

        assertEquals("30,000,000.00", multiplierPage.getResult());
    }

    @Test
    public void millionMultiplier_2() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("1010101")).enterValue2(new BigDecimal("1.89")).andCalculate();

        assertEquals("1,909,090.89", multiplierPage.getResult());
    }

    @Test
    public void millionMultiplier_3() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("12200000")).enterValue2(new BigDecimal("5.50")).andCalculate();

        assertEquals("67,100,000.00", multiplierPage.getResult());
    }

    @Test
    public void millionMultiplier_4() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("2000000")).enterValue2(new BigDecimal("2.5")).andCalculate();

        assertEquals("5,000,000.00", multiplierPage.getResult());
    }

    @Test
    public void millionMultiplier_5() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("33669900")).enterValue2(new BigDecimal("7.5")).andCalculate();

        assertEquals("252,524,250.00", multiplierPage.getResult());
    }

    @AfterClass
    public static void afterClass() {
        BROWSER.close();
    }
}
