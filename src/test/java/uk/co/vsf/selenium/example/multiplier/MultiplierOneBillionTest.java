package uk.co.vsf.selenium.example.multiplier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.co.vsf.selenium.example.Browser;
import uk.co.vsf.selenium.example.page.IndexPage;
import uk.co.vsf.selenium.example.page.MultiplierPage;

public class MultiplierOneBillionTest {

    private static Browser BROWSER;

    private final IndexPage indexPage = new IndexPage(BROWSER);

    @BeforeClass
    public static void beforeClass() {
        // BROWSER = Browser.local(Type.FIREFOX);
        BROWSER = Browser.instance();
    }

    @Before
    public void before() {
        BROWSER.navigateTo(indexPage);
    }

    @Test
    public void billionMultiplier_1() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("4600000000")).enterValue2(new BigDecimal("30.00")).andCalculate();

        assertEquals("138,000,000,000.00", multiplierPage.getResult());
    }

    @Test
    public void billionMultiplier_2() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("2650000000")).enterValue2(new BigDecimal("1.89")).andCalculate();

        assertEquals("5,008,500,000.00", multiplierPage.getResult());
    }

    @Test
    public void billionMultiplier_3() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("1220000000")).enterValue2(new BigDecimal("1.22")).andCalculate();

        assertEquals("1,488,400,000.00", multiplierPage.getResult());
    }

    @Test
    public void billionMultiplier_4() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("3868000000")).enterValue2(new BigDecimal("54.2")).andCalculate();

        assertEquals("209,645,600,000.00", multiplierPage.getResult());
    }

    @Test
    public void billionMultiplier_5() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("20100000000")).enterValue2(new BigDecimal("169.69")).andCalculate();

        assertEquals("3,410,769,000,000.00", multiplierPage.getResult());
    }

    @AfterClass
    public static void afterClass() {
        BROWSER.close();
    }
}
