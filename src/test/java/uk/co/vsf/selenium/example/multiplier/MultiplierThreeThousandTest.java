package uk.co.vsf.selenium.example.multiplier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.co.vsf.selenium.example.Browser;
import uk.co.vsf.selenium.example.page.IndexPage;
import uk.co.vsf.selenium.example.page.MultiplierPage;

public class MultiplierThreeThousandTest {

    private static Browser BROWSER;

    private final IndexPage indexPage = new IndexPage(BROWSER);

    @BeforeClass
    public static void beforeClass() {
        // BROWSER = Browser.local(Type.FIREFOX);
        BROWSER = Browser.instance();
    }

    @Before
    public void before() {
        BROWSER.navigateTo(indexPage);
    }

    @Test
    public void thousandMultiplier_1() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("996633")).enterValue2(new BigDecimal("3.5")).andCalculate();

        assertEquals("3,488,215.50", multiplierPage.getResult());
    }

    @Test
    public void thousandMultiplier_2() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("50000")).enterValue2(new BigDecimal("25.5")).andCalculate();

        assertEquals("1,275,000.00", multiplierPage.getResult());
    }

    @Test
    public void thousandMultiplier_3() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("11500")).enterValue2(new BigDecimal("37.75")).andCalculate();

        assertEquals("434,125.00", multiplierPage.getResult());
    }

    @Test
    public void thousandMultiplier_4() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("1000")).enterValue2(new BigDecimal("1000.00")).andCalculate();

        assertEquals("1,000,000.00", multiplierPage.getResult());
    }

    @Test
    public void thousandMultiplier_5() {
        final MultiplierPage multiplierPage = indexPage.getNavigation().navigateToMultiplierPage();
        assertTrue(multiplierPage.isMultiplierPage());

        multiplierPage.enterValue1(new BigInteger("54321")).enterValue2(new BigDecimal("1.1")).andCalculate();

        assertEquals("59,753.10", multiplierPage.getResult());
    }

    @AfterClass
    public static void afterClass() {
        BROWSER.close();
    }
}
