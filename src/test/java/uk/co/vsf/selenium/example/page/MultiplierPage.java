package uk.co.vsf.selenium.example.page;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import uk.co.vsf.selenium.example.Browser;

public class MultiplierPage extends BasePage {

    @FindBy(how = How.ID, using = "Val1")
    private WebElement val1InputBox;

    @FindBy(how = How.ID, using = "Val2")
    private WebElement val2InputBox;

    @FindBy(how = How.ID, using = "Calculate")
    private WebElement calculateButton;

    @FindBy(how = How.CLASS_NAME, using = "subscr")
    private List<WebElement> footNoteText;

    @FindBy(how = How.ID, using = "Result")
    private WebElement result;

    public MultiplierPage(final Browser browser) {
        super(browser);
    }

    public MultiplierPage enterValue1(final BigInteger val1) {
        if (val1 == null) {
            throw new IllegalArgumentException("value must be more than 1000");
        }

        setVal1(val1);

        return this;
    }

    public void andCalculate() {
        this.calculateButton.click();
        try {
            Thread.sleep(2000);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setVal1(final BigInteger sharesOutstanding) {
        final BigDecimal sharesOutstandingDecimal = new BigDecimal(sharesOutstanding);
        val1InputBox.sendKeys("" + sharesOutstandingDecimal);
    }

    public MultiplierPage enterValue2(final BigDecimal val2) {
        val2InputBox.sendKeys("" + val2);
        return this;
    }

    public boolean isMultiplierPage() {
        return (footNoteText.size() > 1)
                && footNoteText.get(1).getText().contains("The calculations are not guaranteed to be exact");
    }

    public String getResult() {
        return result.getText();
    }

}
